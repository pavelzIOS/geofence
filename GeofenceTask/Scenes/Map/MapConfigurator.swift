//
//  MapConfigurator.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import Foundation

extension MapViewController: MapPresenterOutput { }
extension MapInteractor: MapViewControllerOutput { }
extension MapPresenter: MapInteractorOutput { }

struct MapConfigurator {
    static func scene() -> MapViewController {
        let viewController = MapViewController(nibName: "MapViewController", bundle: nil)
        let presenter = MapPresenter(output: viewController)
        let interactor = MapInteractor(output: presenter)
        viewController.output = interactor
        return viewController
    }
}

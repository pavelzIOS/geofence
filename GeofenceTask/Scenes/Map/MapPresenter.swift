//
//  MapPresenter.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit
import MapKit

// MARK: - Protocols

protocol MapPresenterInput: class {
    func updateWithLocationStatus(_ status: CLAuthorizationStatus)
    func presentLocation(_ location: CLLocation)
    func presentResult(_ result: GeofenceResult, with model: GeofenceViewModel)
}

protocol MapPresenterOutput: class {
    func updateWithLocationStatus(_ status: CLAuthorizationStatus)
    func presentLocation(_ location: CLLocation)
    func presentResult(_ result: GeofenceResult, with location: CLLocation?, radius: Double)
}

// MARK: - Implementation

final class MapPresenter {
    private weak var output: MapPresenterOutput?

    init(output: MapPresenterOutput) {
        self.output = output
    }
}

extension MapPresenter: MapPresenterInput {
    func presentResult(_ result: GeofenceResult, with model: GeofenceViewModel) {
        output?.presentResult(result, with: model.location, radius: model.radius)
    }

    func updateWithLocationStatus(_ status: CLAuthorizationStatus) {
        output?.updateWithLocationStatus(status)
    }

    func presentLocation(_ location: CLLocation) {
        output?.presentLocation(location)
    }
}



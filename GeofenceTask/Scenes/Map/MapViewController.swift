//
//  MapViewController.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerInput {
    func updateWithLocationStatus(_ status: CLAuthorizationStatus)
    func presentLocation(_ location: CLLocation)
    func presentResult(_ result: GeofenceResult, with location: CLLocation?, radius: Double)
}

protocol MapViewControllerOutput {
    func setup()
    func requestLocationAccess()
    func updateMonitoring(location: CLLocation)
    func updateData(ssid: String, radius: Double)
}

class MapViewController: UIViewController {

    var output: MapViewControllerOutput?
    @IBOutlet private var mapView: MKMapView!
    @IBOutlet private var locationAccessView: LocationAccessView!
    @IBOutlet private var filterView: FilterView!
    @IBOutlet private var geofenceStateButton: UIButton!
    private var state: GeofenceResult?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private enum LocalConstants {
        static let pressDuration: Double = 1.0
        static let defaultRadius: Double = 100
        static let mapOffset: Double = 0.0001
        static let regionRatio: Double = 8
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    private func setup() {
        output?.setup()
        setupMap()
        addLongTap()
        setupViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    private func setupViews() {
        filterView.onApplyTapped { [weak self] (data) in
            self?.output?.updateData(ssid: data.networkName, radius: data.radius)
        }
    }

    private func setupMap() {
        mapView.delegate = self
    }

    private func addLongTap() {
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(handleLongTap(gestureRecognizer:)))
        longTap.minimumPressDuration = LocalConstants.pressDuration
        mapView.addGestureRecognizer(longTap)
    }

    @objc private func handleLongTap(gestureRecognizer: UIGestureRecognizer) {
        guard gestureRecognizer.state == .began else { return }

        let touchPoint = gestureRecognizer.location(in: mapView)
        let touchMapCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let touchLocation = CLLocation(latitude: touchMapCoordinate.latitude, longitude: touchMapCoordinate.longitude)

        output?.updateMonitoring(location: touchLocation)
    }

    func addRadiusCircle(location: CLLocation, radius: Double) {
        let circle = MKCircle(center: location.coordinate, radius: radius as CLLocationDistance)
        mapView.removeOverlays(mapView.overlays)
        mapView.addOverlay(circle)
    }
}

extension MapViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = state?.circleColor
        circle.fillColor = state?.circleColor.withAlphaComponent(0.1)
        circle.lineWidth = 1
        return circle
    }
}

extension MapViewController: MapViewControllerInput {

    func presentResult(_ result: GeofenceResult, with location: CLLocation?, radius: Double) {
        state = result
        geofenceStateButton.backgroundColor = result.buttonColor
        geofenceStateButton.setTitle(result.description.capitalized, for: .normal)
        guard let location = location else { return }
        addRadiusCircle(location: location, radius: radius)
        centerMapOnLocation(location: location, radius: radius)
    }

    func updateWithLocationStatus(_ status: CLAuthorizationStatus) {
        locationAccessView.setup(status: status).onAccessAction { [weak self] in
            self?.output?.requestLocationAccess()
        }
    }

    func presentLocation(_ location: CLLocation) {
        centerMapOnLocation(location: location, radius: LocalConstants.defaultRadius)
    }

    func centerMapOnLocation(location: CLLocation, radius: Double) {
        let offsetedLocation = CLLocationCoordinate2D(
            latitude: location.coordinate.latitude + mapView.region.span.latitudeDelta * LocalConstants.mapOffset,
            longitude: location.coordinate.longitude
        )
        let regionRadius: CLLocationDistance = LocalConstants.regionRatio * radius
        let coordinateRegion = MKCoordinateRegion(
            center: offsetedLocation,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius
        )
        mapView.setRegion(coordinateRegion, animated: true)
    }
}

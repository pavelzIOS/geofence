//
//  MapInteractor.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit
import CoreLocation

// MARK: - Protocols

struct GeofenceViewModel {
    var radius: Double
    var location: CLLocation?
    var ssidName: String
    let identifier = "geofence-area"
}

enum GeofenceResult: Equatable, CustomStringConvertible {

    case inside(Bool), outside(Bool)

    var description: String {
        switch self {
        case .inside:
            return "inside"
        case .outside:
            return "outside"
        }
    }

    var buttonColor: UIColor {
        switch self {
        case .inside:
            return .green
        case .outside:
            return .red
        }
    }

    var circleColor: UIColor {
        switch self {
        case .inside(let isInRegion):
            return isInRegion ? .green : .red
        case .outside:
            return .red
        }
    }
}

protocol MapInteractorInput {
    func setup()
    func updateMonitoring(location: CLLocation)
    func updateData(ssid: String, radius: Double)
}

protocol MapInteractorOutput {
    func updateWithLocationStatus(_ status: CLAuthorizationStatus)
    func presentLocation(_ location: CLLocation)
    func presentResult(_ result: GeofenceResult, with model: GeofenceViewModel)
}

// MARK: - Implementation

final class MapInteractor: NSObject {
    private let output: MapInteractorOutput
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation?
    private var viewModel: GeofenceViewModel = GeofenceViewModel(radius: LocalConstants.defaultRadius, location: nil, ssidName: "")
    private var region: CLCircularRegion = CLCircularRegion(center: CLLocationCoordinate2D.init(), radius: LocalConstants.defaultRadius, identifier: "")

    init(output: MapInteractorOutput) {
        self.output = output
    }

    private enum LocalConstants {
        static let defaultRadius: Double = 100
    }
}

extension MapInteractor: MapInteractorInput {

    func requestLocationAccess() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func setup() {
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.delegate = self
    }

    func updateMonitoring(location: CLLocation) {
        viewModel.location = location
        updateMonitoringData()
    }

    func updateData(ssid: String, radius: Double) {
        viewModel.radius = radius
        viewModel.ssidName = ssid
        updateMonitoringData()
    }

    private func updateMonitoringData() {
        locationManager.stopMonitoring(for: region)
        locationManager.monitoredRegions.forEach { locationManager.stopMonitoring(for: $0) }
        if let location = viewModel.location {
            region = CLCircularRegion(
                center: location.coordinate,
                radius: viewModel.radius,
                identifier: viewModel.identifier
            )
            region.notifyOnExit = true
            region.notifyOnEntry = true
            locationManager.startMonitoring(for: region)
        }
        output.presentResult(geofenceStatus(), with: viewModel)
    }

    private func geofenceStatus() -> GeofenceResult {
        let inside = isInRegion() || ssidEquals()
        return inside ? .inside(isInRegion()) : .outside(isInRegion())
    }

    private func ssidEquals() -> Bool {
        guard !viewModel.ssidName.isEmpty else { return false }
        return viewModel.ssidName == UIDevice.current.wifiSSID
    }

    private func isInRegion() -> Bool {
        guard let location = currentLocation else { return false }
        return region.contains(location.coordinate)
    }
}

extension MapInteractor: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .denied:
            print("denied")
        case .notDetermined:
            print("notDetermined")
        case .restricted:
            print("restricted")
        }

        output.updateWithLocationStatus(status)
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        output.presentResult(geofenceStatus(), with: viewModel)
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        output.presentResult(geofenceStatus(), with: viewModel)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        if currentLocation == nil {
            currentLocation = location
            output.presentLocation(location)
        }
    }
}


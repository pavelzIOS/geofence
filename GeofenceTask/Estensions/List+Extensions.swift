//
//  List+Extensions.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit

public protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReusableView { }
extension UITableViewHeaderFooterView: ReusableView { }

public extension UITableView {

    public func register<T: ReusableView>(cellType: T.Type = T.self, bundle: Bundle = Bundle.main) {
        let reuseIdentifier = cellType.defaultReuseIdentifier
        if bundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            register(UINib(nibName: reuseIdentifier, bundle: bundle), forCellReuseIdentifier: reuseIdentifier)
        }
        else {
            register(cellType, forCellReuseIdentifier: reuseIdentifier)
        }
    }

    public func dequeueReusableCell<T>(ofType cellType: T.Type = T.self, at indexPath: IndexPath) -> T where T: UITableViewCell {
        let reuseIdentifier = cellType.defaultReuseIdentifier
        guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(reuseIdentifier)")
        }
        return cell
    }

    public func registerReusableView<E: UITableViewHeaderFooterView>(_ type: E.Type, bundle: Bundle = Bundle.main) {
        let reuseIdentifier = type.defaultReuseIdentifier
        if bundle.path(forResource: reuseIdentifier, ofType: "nib") != nil {
            register(UINib(nibName: reuseIdentifier, bundle: bundle), forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        }
        else {
            register(type, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        }
    }

    public func dequeueReusableHeaderFooterView<E: UITableViewHeaderFooterView>(with type: E.Type) -> E {
        let reuseIdentifier = type.defaultReuseIdentifier
        guard let reusableView = dequeueReusableHeaderFooterView(withIdentifier: reuseIdentifier) as? E else {
            fatalError("Could not dequeue reusable view with identifier: \(reuseIdentifier)")
        }
        return reusableView
    }
}

//
//  UITextField+Extensions.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit

extension UITextField {
    func addDismissToolbar() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Hide", style: UIBarButtonItem.Style.plain, target: self, action: #selector(hide))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        inputAccessoryView = toolBar
    }

    @objc
    private func hide() {
        self.resignFirstResponder()
    }
}

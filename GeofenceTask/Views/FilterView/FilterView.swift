//
//  FilterView.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/10/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit

class FilterView: UIView, XibLoadable {

    typealias FilterData = (radius: Double, networkName: String)
    typealias FilterCallback = (FilterData) -> Void

    var contentView: UIView?
    @IBOutlet private var wifiTextField: UITextField!
    @IBOutlet private var radiusSlider: UISlider!
    @IBOutlet private var radiusLabel: UILabel!

    private var radius: Double = LocalConstants.defaultRadius

    private enum LocalConstants {
        static let defaultRadius: Double = 100
        static let placeholderColor = UIColor.white.withAlphaComponent(0.4)
        static let maximumRadius: Float = 10000
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXib(frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXib(bounds)
        setup()
    }

    private func setup() {
        setupUI()
    }

    private func setupUI() {
        wifiTextField.attributedPlaceholder = NSAttributedString(
            string: "Enter network name",
            attributes: [
                NSAttributedString.Key.foregroundColor: LocalConstants.placeholderColor,
            ]
        )
        wifiTextField.tintColor = UIColor.white
        wifiTextField.addDismissToolbar()
        wifiTextField.delegate = self
        radiusSlider.value = 0.0
        radiusSlider.addTarget(self, action: #selector(sliderChanged(slider:)), for: .valueChanged)
    }

    @objc private func sliderChanged(slider: UISlider) {
        radius = Double(LocalConstants.maximumRadius * slider.value)
        let printRadius = Int(radius)
        radiusLabel.text = "\(printRadius) m"
        onApplyTapped?((radius, wifiTextField.text ?? ""))
    }

    private var onApplyTapped: FilterCallback?
    @discardableResult func onApplyTapped(_ callback: @escaping FilterCallback) -> FilterView {
        wifiTextField.resignFirstResponder()
        onApplyTapped = callback
        return self
    }
}

extension FilterView: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        onApplyTapped?((radius, wifiTextField.text ?? ""))
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" { textField.resignFirstResponder() }
        return true
    }
}



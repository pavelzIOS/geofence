//
//  LocationAccessView.swift
//  GeofenceTask
//
//  Created by Pavel Zagorskiy on 1/9/19.
//  Copyright © 2019 Pavel Zagorskiy. All rights reserved.
//

import UIKit
import MapKit

class LocationAccessView: UIView, XibLoadable {

    typealias AccessCallback = () -> Void

    var contentView: UIView?
    @IBOutlet private var grantAccessButton: UIButton!
    @IBOutlet private var descriptionLabel: UILabel!

    private enum LocalConstants {
        static let animationDuration: Double = 0.2
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupXib(frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXib(bounds)
    }

    @discardableResult func setup(status: CLAuthorizationStatus) -> LocationAccessView {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            showAvailability(hidden: true)
        case .notDetermined:
            descriptionLabel.text = "You need to provide location access to continue"
            grantAccessButton.isHidden = false
            showAvailability(hidden: false)
        case .denied:
            descriptionLabel.text = "Location permission is denied. Check settings"
            grantAccessButton.isHidden = true
            showAvailability(hidden: false)
        case .restricted:
            descriptionLabel.text = "Location permission is denied. Check settings"
            grantAccessButton.isHidden = true
            showAvailability(hidden: false)
        }
        return self
    }

    private func showAvailability(hidden: Bool) {
        let visibility: CGFloat = hidden ? 0.0 : 1.0
        UIView.animate(withDuration: LocalConstants.animationDuration) {
            self.alpha = visibility
        }
    }

    @IBAction func accessAction(_ sender: Any) {
        onAccessTap?()
    }


    private var onAccessTap: AccessCallback?
    @discardableResult func onAccessAction(_ callback: @escaping AccessCallback) -> LocationAccessView {
        onAccessTap = callback
        return self
    }

}
